package de.comparus.opensource.longmap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class LongMapImpl<V> implements LongMap<V> {
    private static final int DEFAULT_CAPACITY = 20;
    private static final float DEFAULT_LOAD = 0.75F;
    private int capacity;
    // load 0.75f is tested from HashMap(optimal)
    private final float load;
    //Maximum value is  2 147 483 647
    private int size = 0;


    private EntryLongMap<Long, V>[] bucket;

    public LongMapImpl(int capacity, float load) {
        if (capacity < 1 || load < 0.1f) {
            throw new IllegalArgumentException("hh");
        }
        this.capacity = capacity;
        this.load = load;
    }

    public LongMapImpl(int capacity) {
        if (capacity < 1) {
            throw new IllegalArgumentException("hh");
        }
        this.load = DEFAULT_LOAD;
        this.capacity = capacity;
    }

    public LongMapImpl() {
        this.capacity = DEFAULT_CAPACITY;
        this.load = DEFAULT_LOAD;
    }


    @SuppressWarnings("unchecked")
    public V put(long key, V value) {
        if (size == 0) {
            create();
        }
        resize();
        EntryLongMap entry = new EntryLongMap(key, value);
        for (EntryLongMap<Long, V> longVEntryLongMap : bucket) {
            if (longVEntryLongMap != null) {
                EntryLongMap<Long, V> obj = longVEntryLongMap;
                boolean hasNext = true;
                while (hasNext) {
                    if (obj.kay.equals(key)) {
                        obj.value = value;
                        return obj.value;
                    } else if (obj.objectNext != null) {
                        obj = obj.objectNext;
                    } else {
                        hasNext = false;
                    }
                }
            }
        }


        int hash = entry.hashCode();
        int index = hash % capacity;
        index = (index < 0 ? -index : index);
        if (bucket[index] == null) {
            bucket[index] = entry;
            size++;
            return (V) entry.value;
        } else {
            EntryLongMap<Long, V> entryPresent = bucket[index];
            while (true) {
                if (entryPresent.objectNext == null) {
                    entryPresent.objectNext = entry;
                    entry.objectBefore = entryPresent;
                    size++;

                    return (V) entry.value;
                } else {
                    entryPresent = entryPresent.objectNext;
                }
            }
        }
    }


    @SuppressWarnings("unchecked")
    public V get(long key) {

        for (EntryLongMap entry : bucket) {
            if (entry != null) {
                if (entry.objectNext == null) {
                    if (entry.kay.equals(key)) {
                        return (V) entry.value;
                    }
                } else {
                    boolean hasNextObject = true;
                    EntryLongMap obj = entry;
                    while (hasNextObject) {
                        if (obj.kay.equals(key)) {
                            return (V) obj.value;
                        }
                        if (obj.objectNext == null) {
                            hasNextObject = false;
                        } else {
                            obj = obj.objectNext;
                        }

                    }
                }
            }
        }
        return null;
    }

    public V remove(long key) {
        for (int i = 0; i < bucket.length; i++) {
            EntryLongMap<Long, V> entry = bucket[i];
            if (entry != null) {
                if (entry.objectNext == null) {
                    if (entry.kay.equals(key)) {
                        bucket[i] = null;
                        size--;
                        return entry.value;
                    }
                } else {
                    boolean hasNextObject = true;
                    while (hasNextObject) {
                        if (entry.kay.equals(key)) {
                            if (entry.objectNext != null && entry.objectBefore != null) {
                                entry.objectBefore.objectNext = entry.objectNext;
                                entry.objectNext.objectBefore = entry.objectBefore;
                            } else {
                                if (entry.objectNext != null) {
                                    entry.objectNext.objectBefore = entry.objectBefore;
                                }
                                if (entry.objectBefore != null) {
                                    entry.objectBefore.objectNext = entry.objectNext;
                                }
                            }
                            size--;
                            return entry.value;
                        }
                        if (entry.objectNext == null) {
                            hasNextObject = false;
                        } else {
                            entry = entry.objectNext;
                        }

                    }
                }
            }
        }
        return null;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean containsKey(long key) {
        long[] keys = keys();
        for (Long k : keys) {
            if (k.equals(key)) {
                return true;
            }
        }

        return false;
    }

    public boolean containsValue(V value) {
        V[] values = values();
        for (V v : values) {
            if (v.equals(value)) {
                return true;
            }
        }
        return false;
    }

    public long[] keys() {
        if (size == 0) {
            return null;
        } else {
            int countKeys = 0;
            long[] keys = new long[size + 1];
            for (int i = 0; i < bucket.length; i++) {
                if (bucket[i] != null) {
                    EntryLongMap<Long, V> entry = bucket[i];
                    boolean hasNext = true;
                    while (hasNext) {
                        if (entry.kay != null) {
                            keys[countKeys] = entry.kay;
                            countKeys++;
                            if (entry.objectNext != null) {
                                entry = entry.objectNext;
                            } else {
                                hasNext = false;
                            }
                        }
                    }
                }
            }
            return keys;
        }
    }

    public V[] values() {
        if (size == 0) {
            return null;
        } else {
            ArrayList<V> arrayList = new ArrayList<>();
            for (int i = 0; i < bucket.length; i++) {
                if (bucket[i] != null) {
                    EntryLongMap<Long, V> entry = bucket[i];
                    boolean hasNext = true;
                    while (hasNext) {
                        if (entry.kay != null) {
                            arrayList.add(entry.value);
                            if (entry.objectNext != null) {
                                entry = entry.objectNext;
                            } else {
                                hasNext = false;
                            }
                        }
                    }
                }
            }
            return toArray(arrayList);
        }

    }

    public long size() {

        return size;

    }

    public void clear() {
        if (size != 0) {
            size = 0;
            Arrays.fill(bucket, null);
        }
    }

    @SuppressWarnings("unchecked")
    private void create() {
        bucket = (EntryLongMap<Long, V>[]) new EntryLongMap[capacity];


    }

    @SuppressWarnings("unchecked")
    private void resize() {
        if (size > capacity * load) {
            int newCapacity = capacity * 2;
            EntryLongMap<Long, V>[] newBucket = (EntryLongMap<Long, V>[]) new EntryLongMap[newCapacity];
            newBucket = Arrays.copyOf(bucket, newCapacity);
            bucket = newBucket;
            capacity = newCapacity;
        }
    }

    @SuppressWarnings("unchecked")
    private static <V> V[] toArray(List<V> list) {
        V[] toArr = (V[]) java.lang.reflect.Array.newInstance(list.get(0)
                .getClass(), list.size());
        for (int i = 0; i < list.size(); i++) {
            toArr[i] = list.get(i);
        }
        return toArr;
    }

    private static class EntryLongMap<Long, V> {
        Long kay;
        V value;
        EntryLongMap<Long, V> objectNext;
        EntryLongMap<Long, V> objectBefore;

        public EntryLongMap(Long kay, V value) {
            this.kay = kay;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (getClass() != o.getClass()) return false;
            EntryLongMap<?, ?> that = (EntryLongMap<?, ?>) o;
            return Objects.equals(kay, that.kay) &&
                    Objects.equals(value, that.value);
        }

        @Override
        public int hashCode() {

            return Objects.hash(kay, value);
        }
    }

}
