package de.comparus.opensource.longmap;

import org.junit.Assert;
import org.junit.Test;

public class LongMapImplTest {

    @Test
    public void testPutOneElement() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        long key = (long) (Math.random() * 100);
        map.put(key, "ONE");
        Assert.assertEquals(map.size(), 1);
    }

    @Test
    public void testPutNullElements() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        long key = (long) (Math.random() * 190);
        map.put(key, null);
        Assert.assertEquals(map.size(), 1);
    }


    @Test
    public void testPut10Elements() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 10; i++) {
            map.put(i, "testing 10 elements");
        }
        Assert.assertEquals(map.size(), 10);

    }


    @Test
    public void testPutOneObjectTwoTimes() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        map.put(67657, "Obj");
        map.put(67657, "Obj");
        Assert.assertEquals(map.size(), 1);

    }

    @Test
    public void testPut10000Elements() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 10000; i++) {
            map.put(i, "testing 10000 elements");
        }
        Assert.assertEquals(map.size(), 10000);
    }

    @Test
    public void testGetByKeyElement() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        map.put(67657, "Obj");
        Assert.assertEquals("Obj", map.get(67657));
        Assert.assertNull(map.get(77678));
    }

    @Test
    public void testGetElementByKeyFrom100Elements() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 100; i++) {
            map.put(i, "testing" + i);
        }
        Assert.assertEquals(map.size(), 100);
        Assert.assertEquals("testing65", map.get(65));
        Assert.assertEquals("testing0", map.get(0));
    }

    @Test
    public void testGetSize() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 100; i++) {
            map.put(i, "testing" + i);
            Assert.assertEquals(i + 1, map.size());
        }
    }

    @Test
    public void testLongMapIsEmpty() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertTrue(map.isEmpty());
        map.put(657656576, "6776");
        Assert.assertFalse(map.isEmpty());
    }


    @Test
    public void testContainsKeyPresent() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 10; i++) {
            map.put(i, "testing" + i);
        }
        Assert.assertTrue(map.containsKey(0));
        Assert.assertTrue(map.containsKey(6));
        Assert.assertTrue(map.containsKey(9));
        Assert.assertFalse(map.containsKey(10));
        Assert.assertEquals(map.size(), 10);
    }

    @Test
    public void testContainsKeyNotPresent() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 10; i++) {
            map.put(i, "testing" + i);
        }
        Assert.assertFalse(map.containsKey(110));
        Assert.assertFalse(map.containsKey(136));
        Assert.assertFalse(map.containsKey(88889));
        Assert.assertEquals(map.size(), 10);
    }

    @Test
    public void testContainsValueNotPresent() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 10; i++) {
            map.put(i, "testing" + i);
        }
        Assert.assertFalse(map.containsValue("testing"));
        Assert.assertFalse(map.containsValue("7testing"));
        Assert.assertFalse(map.containsValue("testing77"));
    }


    @Test
    public void testContainsValuePresent() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 10; i++) {
            map.put(i, "testing" + i);
        }
        Assert.assertTrue(map.containsValue("testing7"));
    }

    @Test
    public void testGetArrayKeys() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 10; i++) {
            map.put(i, "testing" + i);
        }
        long[] keys = map.keys();
        Assert.assertTrue(keys.length != 0);
        Assert.assertEquals(keys.length, 11);
    }

    @Test
    public void testGetArrayValues() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 10; i++) {
            map.put(i, "testing" + i);
        }
        String[] strings = map.values();
        Assert.assertTrue(strings.length != 0);
        Assert.assertEquals(strings.length, 10);
    }

    @Test
    public void testCleatLongMap() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 100; i++) {
            map.put(i, "testing" + i);
        }
        Assert.assertTrue(map.size() != 0);
        Assert.assertEquals(map.size(), 100);
        map.clear();
        Assert.assertEquals(map.size(), 0);
    }


    @Test
    public void testCreateMapMinParam() {
        LongMap<String> map = new LongMapImpl<>(1, 0.1f);
        Assert.assertEquals(map.size(), 0);
        map.put(65465765, " gfgfhg");
        Assert.assertTrue(map.size() != 0);
        map.put(768877786, "hfhg");
        Assert.assertEquals(map.size(), 2);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateMapZeroParam() {
        LongMap<String> map = new LongMapImpl<>(0, 0.0f);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateMapZeroCapacityParam() {
        LongMap<String> map = new LongMapImpl<>(0, 0.1f);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateMapZeroLoadParam() {
        LongMap<String> map = new LongMapImpl<>(100, 0.0f);

    }

    @Test
    public void testDeleteFromMap() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        for (int i = 0; i < 1000; i++) {
            map.put(i, "testing" + i);
        }
        Assert.assertTrue(map.containsKey(88));
        map.remove(88);
        map.remove(0);
        Assert.assertEquals(map.size(), 998);
        Assert.assertFalse(map.containsKey(88));
        Assert.assertEquals(map.keys().length - 1, 998);
    }

    @Test
    public void testDeleteTwoElement() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        map.put(352453, "one");
        map.put(66777877, "two");
        map.remove(352453);
        map.remove(66777877);
        Assert.assertNull(map.keys());
        Assert.assertEquals(map.size(), 0);
    }

    @Test
    public void testDeleteOneElement() {
        LongMap<String> map = new LongMapImpl<>();
        Assert.assertEquals(map.size(), 0);
        map.put(352453, "one");
        map.put(66777877, "two");
        map.remove(352453);
        Assert.assertEquals(map.keys().length - 1, 1);
        Assert.assertEquals(map.size(), 1);
    }

}